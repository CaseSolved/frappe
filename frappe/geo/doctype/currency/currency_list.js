// Copyright (c) 2021, Frappe Technologies and contributors
// For license information, please see license.txt
frappe.listview_settings['Currency'] = {
	hide_name_column: true,
	filters: [
		['enabled', '=', 1]
	]
};
